export const state = () => ({
    user:[]
});

export const getters = {
    user(state) {
        return state.auth.user;
    },

    isLoggedIn(state) {
        return state.auth.loggedIn;
    },
};

export const actions = {
    async getUser(){
        const user = await $nuxt.$axios.get(`/user-profile/`)
        this.commit('updateUser', user)
        return user
    }
};

export const mutations = {
    updateUser(state,user){
        state.user.push({...user})
    }
};
